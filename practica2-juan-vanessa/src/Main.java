import controller.ContenedorPrincipalController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/contenedor-principal.fxml"));
        Parent root = loader.load();

        ContenedorPrincipalController cpc = loader.getController();
        cpc.setStage(primaryStage);
        primaryStage.setTitle("Universidad");
        primaryStage.setScene(new Scene(root, 1200, 700));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

/*
import models.LDLCCNC;

import javax.swing.*;
import java.util.concurrent.ThreadLocalRandom;

 SEGUNDO PRACTICA DE LOGICA II
    Estudiantes:

    Vanessa Tocasuche Ochoa--> CC. 1 040 757 145
    Juan Pablo Arenas -------> CC. 1 037 633 965

    Práctica: 45+65=110 -> 110/8=13.75 -> (0.75)*8=6 -> 6+1=7
    Práctica 7: Listas doblemente ligadas circulares con nodo cabeza.


 */
/*
public class Main {

    public static void main(String[] args) {


        LDLCCNC L2= new LDLCCNC();
        LDLCCNC L3= new LDLCCNC();
        LDLCCNC L5= new LDLCCNC();
        LDLCCNC L0= new LDLCCNC();
        int salir=0;
        do{
            int entrada= Integer.parseInt( JOptionPane.showInputDialog("Ingrese Dato: ") );
            if(entrada%2 == 0){
                L2.insertar(entrada, L2.ultimoNodo());
            }
            if(entrada%3 == 0){
                L3.insertar(entrada, L3.primero);
            }
            if(entrada%5 == 0){
                L5.insertar(entrada, L5.buscaDondeInsertarAscendente(entrada));
            }
            if(entrada%2 != 0 && entrada%3 != 0 && entrada%5 != 0){
                L0.insertar(entrada, L0.buscaDondeInsertarDescendente(entrada));
            }
            salir = JOptionPane.showConfirmDialog(null, "Agregar más nodos?", "", JOptionPane.YES_NO_OPTION);

        }while (salir==0);

        L0.recorreIzqDer();




/*
        int numNodosCreados=0;
        while(numNodosCreados < 5){  //Ciclo donde se crea nodo por nodo y se agrega al final de la lista

            int dato = ThreadLocalRandom.current().nextInt(1, 20 + 1);
            a.insertar( dato, a.ultimoNodo());
            numNodosCreados++;
        }
        a.recorreIzqDer();
        a.recorreDerIzp();



    }

}*/
