package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import models.LDLCCNC;


public class ContenedorPrincipalController {


    private Stage primaryStage;
    @FXML
    private BorderPane contenedorPrincipal;

    @FXML
    public TextField txtNuevoDato;
    @FXML
    private Label lblL0;
    @FXML
    private Label lblL2;
    @FXML
    private Label lblL3;
    @FXML
    private Label lblL5;
    @FXML
    private Label lblL2L3;
    @FXML
    private Label lblL2L5;
    @FXML
    private Label lblL3L5;
    @FXML
    private Label lblL2L3L5;
    @FXML
    private Label lblL2_L3L5;
    @FXML
    private Label lblL3_L2L5;
    @FXML
    private Label lblL5_L2L3;
    @FXML
    private Label lblLZ;
    @FXML
    private Label lblLZorganizada;


    //Listas originales
    LDLCCNC L2= new LDLCCNC();
    LDLCCNC L3= new LDLCCNC();
    LDLCCNC L5= new LDLCCNC();
    LDLCCNC L0= new LDLCCNC();

    //listas interceptadas
    LDLCCNC L2L3= new LDLCCNC();
    LDLCCNC L2L5= new LDLCCNC();
    LDLCCNC L3L5= new LDLCCNC();
    LDLCCNC L2L3L5= new LDLCCNC();

    //Listas excepciones
    LDLCCNC L5_L2L3= new LDLCCNC();
    LDLCCNC L3_L2L5= new LDLCCNC();
    LDLCCNC L2_L3L5= new LDLCCNC();

    //LZ
    LDLCCNC LZ= new LDLCCNC();
    LDLCCNC LZordenada= new LDLCCNC();


    public void btnAgregar_action(ActionEvent actionEvent) {
        try{
            int  entrada= Integer.parseInt(txtNuevoDato.getText().trim());

            LZ.insertar(entrada,LZ.ultimoNodo());
            if(entrada%2 == 0){
                L2.insertar(entrada, L2.ultimoNodo());
                if(entrada%3 == 0)L2L3.insertar(entrada,L2L3.ultimoNodo());
                if(entrada%5 == 0)L2L5.insertar(entrada,L2L5.ultimoNodo());
                if(entrada%5 == 0 && entrada%3 == 0) L2L3L5.insertar(entrada,L2L3L5.ultimoNodo());

                if(entrada%3 != 0 && entrada%5 != 0){
                    L2_L3L5.insertar(entrada,L2_L3L5.ultimoNodo());
                }

            }
            if(entrada%3 == 0){
                L3.insertar(entrada, L3.primero);
                if(entrada%5 == 0) L3L5.insertar(entrada,L3L5.ultimoNodo());

                if(entrada%2 != 0 && entrada%5 != 0){
                    L3_L2L5.insertar(entrada,L3_L2L5.ultimoNodo());
                }
            }
            if(entrada%5 == 0){
                L5.insertar(entrada, L5.buscaDondeInsertarAscendente(entrada));
                if(entrada%2 != 0 && entrada%3 != 0){
                    L5_L2L3.insertar(entrada,L5_L2L3.ultimoNodo());
                }
            }
            if(entrada%2 != 0 && entrada%3 != 0 && entrada%5 != 0){
                L0.insertar(entrada, L0.buscaDondeInsertarDescendente(entrada));
            }

            //demas condiciones
            limpiarCampos();
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("DATO");
            alert.setHeaderText("Dato incorrecto");
            alert.setContentText("El dato debe ser numerico");
            alert.showAndWait();
            return;
        }

    }

    private void limpiarCampos() {
        txtNuevoDato.clear();
    }

    public void btnActualizar_action(ActionEvent actionEvent) {
        lblL0.setText(L0.recorreIzqDer());
        lblL2.setText(L2.recorreIzqDer());
        lblL3.setText(L3.recorreIzqDer());
        lblL5.setText(L5.recorreIzqDer());

        lblL2L3.setText(L2L3.recorreIzqDer());
        lblL2L5.setText(L2L5.recorreIzqDer());
        lblL3L5.setText(L3L5.recorreIzqDer());
        lblL2L3L5.setText(L2L3L5.recorreIzqDer());

        lblL2_L3L5.setText(L2_L3L5.recorreIzqDer());
        lblL3_L2L5.setText(L3_L2L5.recorreIzqDer());
        lblL5_L2L3.setText(L5_L2L3.recorreIzqDer());

        lblLZ.setText(LZ.recorreIzqDer());
        LZordenada= LZ.ordenarPorMitades();
        lblLZorganizada.setText(LZordenada.recorreIzqDer());

    }
    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }



}
