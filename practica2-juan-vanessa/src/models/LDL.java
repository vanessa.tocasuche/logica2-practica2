package models;

public class LDL {

    public NodoDoble primero;
    public NodoDoble ultimo;

    public LDL() {
        this.primero = null;
        this.ultimo = null;
    }

    public boolean finDeRecorrido(NodoDoble p) {   //Retorna true si se llegó al fin de recorrido
        return (p==null);
    }

    public NodoDoble primerNodo(){     //Retorna el primer nodo de la clase NodoSimple
        return primero;
    }

    public NodoDoble ultimoNodo(){     //Retorna el ultimo nodo de la clase NodoSimple
        return ultimo;
    }

    public boolean esVacia() {  //Retorna true si LSL es vacia
        return (primero == null);
    }

    public String recorreIzqDer(){  //recorre la LSL e imprime los datos
        NodoDoble p = primerNodo();
        String muestra="";
        while (!finDeRecorrido(p)){
            if(p==primero){
                //System.out.print("|"+ p.retornaDato()+"|");
                muestra.concat("|"+ p.retornaDato()+"|");
                p = p.retornaLd();
            }else{
                //System.out.print(" -> |"+ p.retornaDato()+"|");
                muestra.concat(" -> |"+ p.retornaDato()+"|");
                p = p.retornaLd();
            }
        }
        return muestra;
    }

    public String recorreDerIzp(){  //recorre la LSL e imprime los datos
        NodoDoble p = ultimoNodo();
        String muestra="";
        while (!finDeRecorrido(p)){
            if(p==ultimo){
                //System.out.print("|"+ p.retornaDato()+"|");
                muestra.concat("|"+ p.retornaDato()+"|");
                p = p.retornaLi();
            }else{
                //System.out.print(" -> |"+ p.retornaDato()+"|");
                muestra.concat(" -> |"+ p.retornaDato()+"|");
                p = p.retornaLi();
            }
        }
        return muestra;
    }

    public NodoDoble buscaDondeInsertarAscendente(Object d){ //Inserta en orden ascendente
        NodoDoble p;

        p= primerNodo();

        while ( !finDeRecorrido(p) &&
                ( (int) p.retornaDato() <= (int) d )){
            p=p.retornaLd();
        }
        return p.retornaLi();
    }
    public NodoDoble buscaDondeInsertarDescendente(Object d){ //Inserta en orden ascendente
        NodoDoble p;

        p= primerNodo();

        while ( !finDeRecorrido(p) &&
                ((int) p.retornaDato() > (int) d ) ){
            p=p.retornaLd();
        }
        return p.retornaLi();
    }

    public void insertar(Object d, NodoDoble y){   //Inserta el objeto d en nuevo nodo despues de y.
                //Cuando se desea insertar de ULTIMO, cuando invoque el metodo poner de entrada  ( d, ultimo )
                //Cuando se desea insertar al INICIO, cuando invoque el metodo poner de entrada  ( d, primero )
        NodoDoble x;
        x= new NodoDoble(d);
        conectar(x, y);
        setTamanioLista();  //Aumenta el tamaño de la lista en el nodo cabeza
    }

    public void conectar(NodoDoble x, NodoDoble y){   //Conectar nodo x con nodo y
        if (y == null) {                    //Conectar al principio
            x.asignaLd(primero);
            if (primero != null) {
                primero.asignaLi(x);
            }else{
                ultimo=y;
            }
            primero=x;
        }
        else if (y.retornaLd()==null){      //Conectar al final
            y.asignaLd(x);
            x.asignaLi(x);
            ultimo=x;
        }
        else{                               //Conecta en el intermedio
            x.asignaLd(y.retornaLd());
            x.asignaLi(y);
            x.retornaLd().asignaLi(x);
            y.asignaLd(x);
        }
    }

    public void desconectar(NodoDoble x){    //Desconectar nodo x
        if (x.retornaLi()==null){                  //Desconectar primer nodo.
            primero=x.retornaLd();
            if (primero==null){
                ultimo=null;
            }else{
                primero.asignaLi(null);
            }
        }
        else if (x.retornaLd()==null){          //Desconectar ultimo nodo
            ultimo = x.retornaLi();
            ultimo.asignaLd(null);
        }
        else{
            x.retornaLd().asignaLi(x.retornaLi());
            x.retornaLi().asignaLd(x.retornaLd());
        }
    }

    public void setTamanioLista(){
        primero.asignaDato( (int) primero.retornaDato() +1);
    }

    public  void tamanioFinal(int t){
        primero.asignaDato(t);
    }


}
