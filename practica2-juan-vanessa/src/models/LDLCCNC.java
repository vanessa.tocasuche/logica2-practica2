package models;

public class LDLCCNC extends LDL {

    public LDLCCNC() {
        NodoDoble nodoCabeza= new NodoDoble(0);
        nodoCabeza.asignaLi(nodoCabeza);
        nodoCabeza.asignaLd(nodoCabeza);
        primero=nodoCabeza;
        ultimo=nodoCabeza;
    }

    public NodoDoble cabeza(){
        return primero;
    }

    public NodoDoble primerNodo(){
        return primero.retornaLd();
    }

    public NodoDoble ultimoNodo(){
        return ultimo;
    }

    @Override
    public boolean esVacia(){
        return primero == primero.retornaLd() && primero == primero.retornaLd();
    }

    @Override
    public boolean finDeRecorrido(NodoDoble p) {   //Retorna true si se llegó al fin de recorrido
        return (p == primero);
    }

    @Override
    public void conectar(NodoDoble x, NodoDoble y){

        if (y == cabeza()) {                    //Conectar al principio
            x.asignaLd(primerNodo());
            x.asignaLi(cabeza());
            if (primerNodo() != cabeza()) {     //Cuando va a ser el primer nodo con dato
                primerNodo().asignaLi(x);
            }else{                              //No es el primer nodo con dato
                ultimo=x;
                primerNodo().asignaLi(ultimo);
            }
            primero.asignaLd(x);
        }
        else if (y.retornaLd()==primero){      //Conectar al final

            y.asignaLd(x);
            x.asignaLi(y);
            x.asignaLd(primero);
            ultimo=x;
            cabeza().asignaLi(ultimo);

        }
        else{                               //Conecta en el intermedio
            x.asignaLd(y.retornaLd());
            x.asignaLi(y);
            x.retornaLd().asignaLi(x);
            y.asignaLd(x);
        }
    }

    @Override
    public void desconectar(NodoDoble x){           //**************probar funcionalidad
        if(x==ultimo){
            ultimo= x.retornaLi();
        }else if(x==cabeza()){
            System.out.println("No se puede eliminar nodo cabeza");
            return;
        }
        NodoDoble aux= x.retornaLi();
        x.retornaLi().asignaLd(x);
        x.retornaLd().asignaLi(aux);

    }

    @Override
    public String recorreIzqDer(){  //recorre la LSL e imprime los datos

        NodoDoble p = primero.retornaLd();
        //System.out.print("|"+ primero.retornaDato()+"|");
        String muestra="|"+ primero.retornaDato()+"|";
        while (p!=primero){
            //System.out.print(" -> |"+ p.retornaDato()+"|");
            muestra=muestra.concat(" -> |"+ p.retornaDato()+"|");
            p = p.retornaLd();
        }
        //System.out.print("\n");
        muestra.concat("\n");

        return muestra;
    }

    @Override
    public String recorreDerIzp(){
        NodoDoble p = ultimoNodo();
        String muestra="";
        do{
            //System.out.print("|"+ p.retornaDato()+"| -> ");
            muestra.concat("|"+ p.retornaDato()+"| -> ");
            p = p.retornaLi();
        }while (!finDeRecorrido(p));
        //System.out.print("|"+ primero.retornaDato()+"|");
        muestra.concat("|"+ primero.retornaDato()+"|");
        return muestra;
    }

    public LDLCCNC ordenarPorMitades(){  //Ordena la primera mitad de forma ascendente y la segunda mitad de manera descendente
        int mitad = (int) this.primero.retornaDato()/2;
        NodoDoble p = this.primero.retornaLd();
        LDLCCNC lzPrimeraMitad = new LDLCCNC();
        LDLCCNC lzSegundaMitad = new LDLCCNC();
        int i;
        for (i = 1; i <= mitad ; i++) {
            lzPrimeraMitad.insertar(p.retornaDato(), lzPrimeraMitad.buscaDondeInsertarAscendente(p.retornaDato()));
            p = p.retornaLd();
        }
        for (int j = i; j <= (int)this.cabeza().retornaDato() ; j++) {
            lzSegundaMitad.insertar(p.retornaDato(), lzSegundaMitad.buscaDondeInsertarDescendente(p.retornaDato()));
            p = p.retornaLd();
        }
        lzPrimeraMitad.ultimoNodo().asignaLd(lzSegundaMitad.primerNodo());
        lzPrimeraMitad.primero.asignaLi(lzSegundaMitad.ultimoNodo());
        lzSegundaMitad.primerNodo().asignaLi(lzPrimeraMitad.ultimoNodo());
        lzSegundaMitad.ultimo.asignaLd(lzPrimeraMitad.primero);
        lzPrimeraMitad.tamanioFinal( (int) this.cabeza().retornaDato() );

        return lzPrimeraMitad;
    }






}
